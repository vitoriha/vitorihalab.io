---
layout: post
title:  "Hacktoberfest"
author: iha
categories: coding opensource hacktoberfest
image: /images/hacktoberfest_status.png
---


This is my hacktoberfest status. For me it was a way to looking for open source projects to contribute.

I contributed with small things just to know how the open source communities work. My patches were just a kind of hello world.

I've been studying many others open source projects to contribute, such as Kernel, Zephyr, Linaro, X and GStreamer.

Yesterday I sent a patch to Kernel DRM Subsystem, following all the requirements to do it, and my patch was acked! Of course it was a checkpatch issue, but there are many things to read before send anything. And I hope as soon as possible I'll be contributing with more challenging things.
